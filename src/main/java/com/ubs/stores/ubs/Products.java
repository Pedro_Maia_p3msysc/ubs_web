/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author plpm
 */
@Entity
@Table(name = "Products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Products.findAll", query = "SELECT p FROM Products p"),
    @NamedQuery(name = "Products.findByProduct", query = "SELECT p FROM Products p WHERE p.productsPK.product = :product"),
    @NamedQuery(name = "Products.findByFileName", query = "SELECT p FROM Products p WHERE p.productsPK.fileName = :fileName"),
    @NamedQuery(name = "Products.findByQuantity", query = "SELECT p FROM Products p WHERE p.quantity = :quantity"),
    @NamedQuery(name = "Products.findByPrice", query = "SELECT p FROM Products p WHERE p.price = :price"),
    @NamedQuery(name = "Products.findByType", query = "SELECT p FROM Products p WHERE p.type = :type"),
    @NamedQuery(name = "Products.findByIndustry", query = "SELECT p FROM Products p WHERE p.industry = :industry"),
    @NamedQuery(name = "Products.findByOrigin", query = "SELECT p FROM Products p WHERE p.origin = :origin")})
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductsPK productsPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private int quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private BigDecimal price;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "industry")
    private String industry;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "origin")
    private String origin;
    @JoinColumn(name = "fileName", referencedColumnName = "fileName", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Files files;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "products")
    private Collection<StoresHasProducts> storesHasProductsCollection;

    public Products() {
    }

    public Products(ProductsPK productsPK) {
        this.productsPK = productsPK;
    }

    public Products(ProductsPK productsPK, int quantity, BigDecimal price, String type, String industry, String origin) {
        this.productsPK = productsPK;
        this.quantity = quantity;
        this.price = price;
        this.type = type;
        this.industry = industry;
        this.origin = origin;
    }

    public Products(String product, String fileName) {
        this.productsPK = new ProductsPK(product, fileName);
    }

    public ProductsPK getProductsPK() {
        return productsPK;
    }

    public void setProductsPK(ProductsPK productsPK) {
        this.productsPK = productsPK;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    @XmlTransient
    public Collection<StoresHasProducts> getStoresHasProductsCollection() {
        return storesHasProductsCollection;
    }

    public void setStoresHasProductsCollection(Collection<StoresHasProducts> storesHasProductsCollection) {
        this.storesHasProductsCollection = storesHasProductsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productsPK != null ? productsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Products)) {
            return false;
        }
        Products other = (Products) object;
        if ((this.productsPK == null && other.productsPK != null) || (this.productsPK != null && !this.productsPK.equals(other.productsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ubs.stores.ubs.Products[ productsPK=" + productsPK + " ]";
    }
    
}
