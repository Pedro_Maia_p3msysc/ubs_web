/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author plpm
 */
@Entity
@Table(name = "Stores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stores.findAll", query = "SELECT s FROM Stores s"),
    @NamedQuery(name = "Stores.findByStoreName", query = "SELECT s FROM Stores s WHERE s.storeName = :storeName"),
    @NamedQuery(name = "Stores.findByDescription", query = "SELECT s FROM Stores s WHERE s.description = :description")})
public class Stores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "storeName")
    private String storeName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stores")
    private Collection<StoresHasProducts> storesHasProductsCollection;

    public Stores() {
    }

    public Stores(String storeName) {
        this.storeName = storeName;
    }

    public Stores(String storeName, String description) {
        this.storeName = storeName;
        this.description = description;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<StoresHasProducts> getStoresHasProductsCollection() {
        return storesHasProductsCollection;
    }

    public void setStoresHasProductsCollection(Collection<StoresHasProducts> storesHasProductsCollection) {
        this.storesHasProductsCollection = storesHasProductsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (storeName != null ? storeName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stores)) {
            return false;
        }
        Stores other = (Stores) object;
        if ((this.storeName == null && other.storeName != null) || (this.storeName != null && !this.storeName.equals(other.storeName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ubs.stores.ubs.Stores[ storeName=" + storeName + " ]";
    }
    
}
