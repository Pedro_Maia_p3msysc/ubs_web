/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import com.ubs.stores.ubs.exceptions.NonexistentEntityException;
import com.ubs.stores.ubs.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author plpm
 */
public class StoresHasProductsJpaController implements Serializable {

    public StoresHasProductsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StoresHasProducts storesHasProducts) throws PreexistingEntityException, Exception {
        if (storesHasProducts.getStoresHasProductsPK() == null) {
            storesHasProducts.setStoresHasProductsPK(new StoresHasProductsPK());
        }
        storesHasProducts.getStoresHasProductsPK().setStoreName(storesHasProducts.getStores().getStoreName());
        storesHasProducts.getStoresHasProductsPK().setFileName(storesHasProducts.getProducts().getProductsPK().getFileName());
        storesHasProducts.getStoresHasProductsPK().setProduct(storesHasProducts.getProducts().getProductsPK().getProduct());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Products products = storesHasProducts.getProducts();
            if (products != null) {
                products = em.getReference(products.getClass(), products.getProductsPK());
                storesHasProducts.setProducts(products);
            }
            Stores stores = storesHasProducts.getStores();
            if (stores != null) {
                stores = em.getReference(stores.getClass(), stores.getStoreName());
                storesHasProducts.setStores(stores);
            }
            em.persist(storesHasProducts);
            if (products != null) {
                products.getStoresHasProductsCollection().add(storesHasProducts);
                products = em.merge(products);
            }
            if (stores != null) {
                stores.getStoresHasProductsCollection().add(storesHasProducts);
                stores = em.merge(stores);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findStoresHasProducts(storesHasProducts.getStoresHasProductsPK()) != null) {
                throw new PreexistingEntityException("StoresHasProducts " + storesHasProducts + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StoresHasProducts storesHasProducts) throws NonexistentEntityException, Exception {
        storesHasProducts.getStoresHasProductsPK().setStoreName(storesHasProducts.getStores().getStoreName());
        storesHasProducts.getStoresHasProductsPK().setFileName(storesHasProducts.getProducts().getProductsPK().getFileName());
        storesHasProducts.getStoresHasProductsPK().setProduct(storesHasProducts.getProducts().getProductsPK().getProduct());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StoresHasProducts persistentStoresHasProducts = em.find(StoresHasProducts.class, storesHasProducts.getStoresHasProductsPK());
            Products productsOld = persistentStoresHasProducts.getProducts();
            Products productsNew = storesHasProducts.getProducts();
            Stores storesOld = persistentStoresHasProducts.getStores();
            Stores storesNew = storesHasProducts.getStores();
            if (productsNew != null) {
                productsNew = em.getReference(productsNew.getClass(), productsNew.getProductsPK());
                storesHasProducts.setProducts(productsNew);
            }
            if (storesNew != null) {
                storesNew = em.getReference(storesNew.getClass(), storesNew.getStoreName());
                storesHasProducts.setStores(storesNew);
            }
            storesHasProducts = em.merge(storesHasProducts);
            if (productsOld != null && !productsOld.equals(productsNew)) {
                productsOld.getStoresHasProductsCollection().remove(storesHasProducts);
                productsOld = em.merge(productsOld);
            }
            if (productsNew != null && !productsNew.equals(productsOld)) {
                productsNew.getStoresHasProductsCollection().add(storesHasProducts);
                productsNew = em.merge(productsNew);
            }
            if (storesOld != null && !storesOld.equals(storesNew)) {
                storesOld.getStoresHasProductsCollection().remove(storesHasProducts);
                storesOld = em.merge(storesOld);
            }
            if (storesNew != null && !storesNew.equals(storesOld)) {
                storesNew.getStoresHasProductsCollection().add(storesHasProducts);
                storesNew = em.merge(storesNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                StoresHasProductsPK id = storesHasProducts.getStoresHasProductsPK();
                if (findStoresHasProducts(id) == null) {
                    throw new NonexistentEntityException("The storesHasProducts with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(StoresHasProductsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StoresHasProducts storesHasProducts;
            try {
                storesHasProducts = em.getReference(StoresHasProducts.class, id);
                storesHasProducts.getStoresHasProductsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The storesHasProducts with id " + id + " no longer exists.", enfe);
            }
            Products products = storesHasProducts.getProducts();
            if (products != null) {
                products.getStoresHasProductsCollection().remove(storesHasProducts);
                products = em.merge(products);
            }
            Stores stores = storesHasProducts.getStores();
            if (stores != null) {
                stores.getStoresHasProductsCollection().remove(storesHasProducts);
                stores = em.merge(stores);
            }
            em.remove(storesHasProducts);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StoresHasProducts> findStoresHasProductsEntities() {
        return findStoresHasProductsEntities(true, -1, -1);
    }

    public List<StoresHasProducts> findStoresHasProductsEntities(int maxResults, int firstResult) {
        return findStoresHasProductsEntities(false, maxResults, firstResult);
    }

    private List<StoresHasProducts> findStoresHasProductsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StoresHasProducts.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StoresHasProducts findStoresHasProducts(StoresHasProductsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StoresHasProducts.class, id);
        } finally {
            em.close();
        }
    }

    public int getStoresHasProductsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StoresHasProducts> rt = cq.from(StoresHasProducts.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
