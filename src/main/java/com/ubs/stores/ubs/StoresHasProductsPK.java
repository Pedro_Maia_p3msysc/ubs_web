/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author plpm
 */
@Embeddable
public class StoresHasProductsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "product")
    private String product;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "fileName")
    private String fileName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "storeName")
    private String storeName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sequence")
    private int sequence;

    public StoresHasProductsPK() {
    }

    public StoresHasProductsPK(String product, String fileName, String storeName, int sequence) {
        this.product = product;
        this.fileName = fileName;
        this.storeName = storeName;
        this.sequence = sequence;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (product != null ? product.hashCode() : 0);
        hash += (fileName != null ? fileName.hashCode() : 0);
        hash += (storeName != null ? storeName.hashCode() : 0);
        hash += (int) sequence;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StoresHasProductsPK)) {
            return false;
        }
        StoresHasProductsPK other = (StoresHasProductsPK) object;
        if ((this.product == null && other.product != null) || (this.product != null && !this.product.equals(other.product))) {
            return false;
        }
        if ((this.fileName == null && other.fileName != null) || (this.fileName != null && !this.fileName.equals(other.fileName))) {
            return false;
        }
        if ((this.storeName == null && other.storeName != null) || (this.storeName != null && !this.storeName.equals(other.storeName))) {
            return false;
        }
        if (this.sequence != other.sequence) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ubs.stores.ubs.StoresHasProductsPK[ product=" + product + ", fileName=" + fileName + ", storeName=" + storeName + ", sequence=" + sequence + " ]";
    }
    
}
