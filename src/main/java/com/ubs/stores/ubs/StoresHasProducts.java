/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author plpm
 */
@Entity
@Table(name = "StoresHasProducts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StoresHasProducts.findAll", query = "SELECT s FROM StoresHasProducts s"),
    @NamedQuery(name = "StoresHasProducts.findByProduct", query = "SELECT s FROM StoresHasProducts s WHERE s.storesHasProductsPK.product = :product"),
    @NamedQuery(name = "StoresHasProducts.findByFileName", query = "SELECT s FROM StoresHasProducts s WHERE s.storesHasProductsPK.fileName = :fileName"),
    @NamedQuery(name = "StoresHasProducts.findByStoreName", query = "SELECT s FROM StoresHasProducts s WHERE s.storesHasProductsPK.storeName = :storeName"),
    @NamedQuery(name = "StoresHasProducts.findBySequence", query = "SELECT s FROM StoresHasProducts s WHERE s.storesHasProductsPK.sequence = :sequence"),
    @NamedQuery(name = "StoresHasProducts.findByAcquiredPrice", query = "SELECT s FROM StoresHasProducts s WHERE s.acquiredPrice = :acquiredPrice"),
    @NamedQuery(name = "StoresHasProducts.findByQuantity", query = "SELECT s FROM StoresHasProducts s WHERE s.quantity = :quantity")})
public class StoresHasProducts implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected StoresHasProductsPK storesHasProductsPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "acquiredPrice")
    private BigDecimal acquiredPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private int quantity;
    @JoinColumns({
        @JoinColumn(name = "product", referencedColumnName = "product", insertable = false, updatable = false),
        @JoinColumn(name = "fileName", referencedColumnName = "fileName", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Products products;
    @JoinColumn(name = "storeName", referencedColumnName = "storeName", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Stores stores;

    public StoresHasProducts() {
    }

    public StoresHasProducts(StoresHasProductsPK storesHasProductsPK) {
        this.storesHasProductsPK = storesHasProductsPK;
    }

    public StoresHasProducts(StoresHasProductsPK storesHasProductsPK, BigDecimal acquiredPrice, int quantity) {
        this.storesHasProductsPK = storesHasProductsPK;
        this.acquiredPrice = acquiredPrice;
        this.quantity = quantity;
    }

    public StoresHasProducts(String product, String fileName, String storeName, int sequence) {
        this.storesHasProductsPK = new StoresHasProductsPK(product, fileName, storeName, sequence);
    }

    public StoresHasProductsPK getStoresHasProductsPK() {
        return storesHasProductsPK;
    }

    public void setStoresHasProductsPK(StoresHasProductsPK storesHasProductsPK) {
        this.storesHasProductsPK = storesHasProductsPK;
    }

    public BigDecimal getAcquiredPrice() {
        return acquiredPrice;
    }

    public void setAcquiredPrice(BigDecimal acquiredPrice) {
        this.acquiredPrice = acquiredPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Stores getStores() {
        return stores;
    }

    public void setStores(Stores stores) {
        this.stores = stores;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (storesHasProductsPK != null ? storesHasProductsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StoresHasProducts)) {
            return false;
        }
        StoresHasProducts other = (StoresHasProducts) object;
        if ((this.storesHasProductsPK == null && other.storesHasProductsPK != null) || (this.storesHasProductsPK != null && !this.storesHasProductsPK.equals(other.storesHasProductsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ubs.stores.ubs.StoresHasProducts[ storesHasProductsPK=" + storesHasProductsPK + " ]";
    }
    
}
