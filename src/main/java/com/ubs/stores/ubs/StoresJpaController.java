/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import com.ubs.stores.ubs.exceptions.IllegalOrphanException;
import com.ubs.stores.ubs.exceptions.NonexistentEntityException;
import com.ubs.stores.ubs.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author plpm
 */
public class StoresJpaController implements Serializable {

    public StoresJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Stores stores) throws PreexistingEntityException, Exception {
        if (stores.getStoresHasProductsCollection() == null) {
            stores.setStoresHasProductsCollection(new ArrayList<StoresHasProducts>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<StoresHasProducts> attachedStoresHasProductsCollection = new ArrayList<StoresHasProducts>();
            for (StoresHasProducts storesHasProductsCollectionStoresHasProductsToAttach : stores.getStoresHasProductsCollection()) {
                storesHasProductsCollectionStoresHasProductsToAttach = em.getReference(storesHasProductsCollectionStoresHasProductsToAttach.getClass(), storesHasProductsCollectionStoresHasProductsToAttach.getStoresHasProductsPK());
                attachedStoresHasProductsCollection.add(storesHasProductsCollectionStoresHasProductsToAttach);
            }
            stores.setStoresHasProductsCollection(attachedStoresHasProductsCollection);
            em.persist(stores);
            for (StoresHasProducts storesHasProductsCollectionStoresHasProducts : stores.getStoresHasProductsCollection()) {
                Stores oldStoresOfStoresHasProductsCollectionStoresHasProducts = storesHasProductsCollectionStoresHasProducts.getStores();
                storesHasProductsCollectionStoresHasProducts.setStores(stores);
                storesHasProductsCollectionStoresHasProducts = em.merge(storesHasProductsCollectionStoresHasProducts);
                if (oldStoresOfStoresHasProductsCollectionStoresHasProducts != null) {
                    oldStoresOfStoresHasProductsCollectionStoresHasProducts.getStoresHasProductsCollection().remove(storesHasProductsCollectionStoresHasProducts);
                    oldStoresOfStoresHasProductsCollectionStoresHasProducts = em.merge(oldStoresOfStoresHasProductsCollectionStoresHasProducts);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findStores(stores.getStoreName()) != null) {
                throw new PreexistingEntityException("Stores " + stores + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Stores stores) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Stores persistentStores = em.find(Stores.class, stores.getStoreName());
            Collection<StoresHasProducts> storesHasProductsCollectionOld = persistentStores.getStoresHasProductsCollection();
            Collection<StoresHasProducts> storesHasProductsCollectionNew = stores.getStoresHasProductsCollection();
            List<String> illegalOrphanMessages = null;
            for (StoresHasProducts storesHasProductsCollectionOldStoresHasProducts : storesHasProductsCollectionOld) {
                if (!storesHasProductsCollectionNew.contains(storesHasProductsCollectionOldStoresHasProducts)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StoresHasProducts " + storesHasProductsCollectionOldStoresHasProducts + " since its stores field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<StoresHasProducts> attachedStoresHasProductsCollectionNew = new ArrayList<StoresHasProducts>();
            for (StoresHasProducts storesHasProductsCollectionNewStoresHasProductsToAttach : storesHasProductsCollectionNew) {
                storesHasProductsCollectionNewStoresHasProductsToAttach = em.getReference(storesHasProductsCollectionNewStoresHasProductsToAttach.getClass(), storesHasProductsCollectionNewStoresHasProductsToAttach.getStoresHasProductsPK());
                attachedStoresHasProductsCollectionNew.add(storesHasProductsCollectionNewStoresHasProductsToAttach);
            }
            storesHasProductsCollectionNew = attachedStoresHasProductsCollectionNew;
            stores.setStoresHasProductsCollection(storesHasProductsCollectionNew);
            stores = em.merge(stores);
            for (StoresHasProducts storesHasProductsCollectionNewStoresHasProducts : storesHasProductsCollectionNew) {
                if (!storesHasProductsCollectionOld.contains(storesHasProductsCollectionNewStoresHasProducts)) {
                    Stores oldStoresOfStoresHasProductsCollectionNewStoresHasProducts = storesHasProductsCollectionNewStoresHasProducts.getStores();
                    storesHasProductsCollectionNewStoresHasProducts.setStores(stores);
                    storesHasProductsCollectionNewStoresHasProducts = em.merge(storesHasProductsCollectionNewStoresHasProducts);
                    if (oldStoresOfStoresHasProductsCollectionNewStoresHasProducts != null && !oldStoresOfStoresHasProductsCollectionNewStoresHasProducts.equals(stores)) {
                        oldStoresOfStoresHasProductsCollectionNewStoresHasProducts.getStoresHasProductsCollection().remove(storesHasProductsCollectionNewStoresHasProducts);
                        oldStoresOfStoresHasProductsCollectionNewStoresHasProducts = em.merge(oldStoresOfStoresHasProductsCollectionNewStoresHasProducts);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = stores.getStoreName();
                if (findStores(id) == null) {
                    throw new NonexistentEntityException("The stores with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Stores stores;
            try {
                stores = em.getReference(Stores.class, id);
                stores.getStoreName();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stores with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StoresHasProducts> storesHasProductsCollectionOrphanCheck = stores.getStoresHasProductsCollection();
            for (StoresHasProducts storesHasProductsCollectionOrphanCheckStoresHasProducts : storesHasProductsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Stores (" + stores + ") cannot be destroyed since the StoresHasProducts " + storesHasProductsCollectionOrphanCheckStoresHasProducts + " in its storesHasProductsCollection field has a non-nullable stores field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(stores);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Stores> findStoresEntities() {
        return findStoresEntities(true, -1, -1);
    }

    public List<Stores> findStoresEntities(int maxResults, int firstResult) {
        return findStoresEntities(false, maxResults, firstResult);
    }

    private List<Stores> findStoresEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Stores.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Stores findStores(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Stores.class, id);
        } finally {
            em.close();
        }
    }

    public int getStoresCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Stores> rt = cq.from(Stores.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
