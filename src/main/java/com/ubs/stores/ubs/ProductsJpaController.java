/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import com.ubs.stores.ubs.exceptions.IllegalOrphanException;
import com.ubs.stores.ubs.exceptions.NonexistentEntityException;
import com.ubs.stores.ubs.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author plpm
 */
public class ProductsJpaController implements Serializable {

    public ProductsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Products products) throws PreexistingEntityException, Exception {
        if (products.getProductsPK() == null) {
            products.setProductsPK(new ProductsPK());
        }
        if (products.getStoresHasProductsCollection() == null) {
            products.setStoresHasProductsCollection(new ArrayList<StoresHasProducts>());
        }
        products.getProductsPK().setFileName(products.getFiles().getFileName());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Files files = products.getFiles();
            if (files != null) {
                files = em.getReference(files.getClass(), files.getFileName());
                products.setFiles(files);
            }
            Collection<StoresHasProducts> attachedStoresHasProductsCollection = new ArrayList<StoresHasProducts>();
            for (StoresHasProducts storesHasProductsCollectionStoresHasProductsToAttach : products.getStoresHasProductsCollection()) {
                storesHasProductsCollectionStoresHasProductsToAttach = em.getReference(storesHasProductsCollectionStoresHasProductsToAttach.getClass(), storesHasProductsCollectionStoresHasProductsToAttach.getStoresHasProductsPK());
                attachedStoresHasProductsCollection.add(storesHasProductsCollectionStoresHasProductsToAttach);
            }
            products.setStoresHasProductsCollection(attachedStoresHasProductsCollection);
            em.persist(products);
            if (files != null) {
                files.getProductsCollection().add(products);
                files = em.merge(files);
            }
            for (StoresHasProducts storesHasProductsCollectionStoresHasProducts : products.getStoresHasProductsCollection()) {
                Products oldProductsOfStoresHasProductsCollectionStoresHasProducts = storesHasProductsCollectionStoresHasProducts.getProducts();
                storesHasProductsCollectionStoresHasProducts.setProducts(products);
                storesHasProductsCollectionStoresHasProducts = em.merge(storesHasProductsCollectionStoresHasProducts);
                if (oldProductsOfStoresHasProductsCollectionStoresHasProducts != null) {
                    oldProductsOfStoresHasProductsCollectionStoresHasProducts.getStoresHasProductsCollection().remove(storesHasProductsCollectionStoresHasProducts);
                    oldProductsOfStoresHasProductsCollectionStoresHasProducts = em.merge(oldProductsOfStoresHasProductsCollectionStoresHasProducts);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProducts(products.getProductsPK()) != null) {
                throw new PreexistingEntityException("Products " + products + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Products products) throws IllegalOrphanException, NonexistentEntityException, Exception {
        products.getProductsPK().setFileName(products.getFiles().getFileName());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Products persistentProducts = em.find(Products.class, products.getProductsPK());
            Files filesOld = persistentProducts.getFiles();
            Files filesNew = products.getFiles();
            Collection<StoresHasProducts> storesHasProductsCollectionOld = persistentProducts.getStoresHasProductsCollection();
            Collection<StoresHasProducts> storesHasProductsCollectionNew = products.getStoresHasProductsCollection();
            List<String> illegalOrphanMessages = null;
            for (StoresHasProducts storesHasProductsCollectionOldStoresHasProducts : storesHasProductsCollectionOld) {
                if (!storesHasProductsCollectionNew.contains(storesHasProductsCollectionOldStoresHasProducts)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StoresHasProducts " + storesHasProductsCollectionOldStoresHasProducts + " since its products field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (filesNew != null) {
                filesNew = em.getReference(filesNew.getClass(), filesNew.getFileName());
                products.setFiles(filesNew);
            }
            Collection<StoresHasProducts> attachedStoresHasProductsCollectionNew = new ArrayList<StoresHasProducts>();
            for (StoresHasProducts storesHasProductsCollectionNewStoresHasProductsToAttach : storesHasProductsCollectionNew) {
                storesHasProductsCollectionNewStoresHasProductsToAttach = em.getReference(storesHasProductsCollectionNewStoresHasProductsToAttach.getClass(), storesHasProductsCollectionNewStoresHasProductsToAttach.getStoresHasProductsPK());
                attachedStoresHasProductsCollectionNew.add(storesHasProductsCollectionNewStoresHasProductsToAttach);
            }
            storesHasProductsCollectionNew = attachedStoresHasProductsCollectionNew;
            products.setStoresHasProductsCollection(storesHasProductsCollectionNew);
            products = em.merge(products);
            if (filesOld != null && !filesOld.equals(filesNew)) {
                filesOld.getProductsCollection().remove(products);
                filesOld = em.merge(filesOld);
            }
            if (filesNew != null && !filesNew.equals(filesOld)) {
                filesNew.getProductsCollection().add(products);
                filesNew = em.merge(filesNew);
            }
            for (StoresHasProducts storesHasProductsCollectionNewStoresHasProducts : storesHasProductsCollectionNew) {
                if (!storesHasProductsCollectionOld.contains(storesHasProductsCollectionNewStoresHasProducts)) {
                    Products oldProductsOfStoresHasProductsCollectionNewStoresHasProducts = storesHasProductsCollectionNewStoresHasProducts.getProducts();
                    storesHasProductsCollectionNewStoresHasProducts.setProducts(products);
                    storesHasProductsCollectionNewStoresHasProducts = em.merge(storesHasProductsCollectionNewStoresHasProducts);
                    if (oldProductsOfStoresHasProductsCollectionNewStoresHasProducts != null && !oldProductsOfStoresHasProductsCollectionNewStoresHasProducts.equals(products)) {
                        oldProductsOfStoresHasProductsCollectionNewStoresHasProducts.getStoresHasProductsCollection().remove(storesHasProductsCollectionNewStoresHasProducts);
                        oldProductsOfStoresHasProductsCollectionNewStoresHasProducts = em.merge(oldProductsOfStoresHasProductsCollectionNewStoresHasProducts);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ProductsPK id = products.getProductsPK();
                if (findProducts(id) == null) {
                    throw new NonexistentEntityException("The products with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ProductsPK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Products products;
            try {
                products = em.getReference(Products.class, id);
                products.getProductsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The products with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StoresHasProducts> storesHasProductsCollectionOrphanCheck = products.getStoresHasProductsCollection();
            for (StoresHasProducts storesHasProductsCollectionOrphanCheckStoresHasProducts : storesHasProductsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Products (" + products + ") cannot be destroyed since the StoresHasProducts " + storesHasProductsCollectionOrphanCheckStoresHasProducts + " in its storesHasProductsCollection field has a non-nullable products field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Files files = products.getFiles();
            if (files != null) {
                files.getProductsCollection().remove(products);
                files = em.merge(files);
            }
            em.remove(products);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Products> findProductsEntities() {
        return findProductsEntities(true, -1, -1);
    }

    public List<Products> findProductsEntities(int maxResults, int firstResult) {
        return findProductsEntities(false, maxResults, firstResult);
    }

    private List<Products> findProductsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Products.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Products findProducts(ProductsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Products.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Products> rt = cq.from(Products.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
