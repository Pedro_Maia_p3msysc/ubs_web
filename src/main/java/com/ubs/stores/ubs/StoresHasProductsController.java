package com.ubs.stores.ubs;

import com.ubs.stores.ubs.util.JsfUtil;
import com.ubs.stores.ubs.util.PaginationHelper;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("storesHasProductsController")
@SessionScoped
public class StoresHasProductsController implements Serializable {

    private StoresHasProducts current;
    private DataModel items = null;
    @EJB
    private com.ubs.stores.ubs.StoresHasProductsFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public StoresHasProductsController() {
    }

    public StoresHasProducts getSelected() {
        if (current == null) {
            current = new StoresHasProducts();
            current.setStoresHasProductsPK(new com.ubs.stores.ubs.StoresHasProductsPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private StoresHasProductsFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (StoresHasProducts) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new StoresHasProducts();
        current.setStoresHasProductsPK(new com.ubs.stores.ubs.StoresHasProductsPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getStoresHasProductsPK().setProduct(current.getProducts().getProductsPK().getProduct());
            current.getStoresHasProductsPK().setFileName(current.getProducts().getProductsPK().getFileName());
            current.getStoresHasProductsPK().setStoreName(current.getStores().getStoreName());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("StoresHasProductsCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (StoresHasProducts) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getStoresHasProductsPK().setProduct(current.getProducts().getProductsPK().getProduct());
            current.getStoresHasProductsPK().setFileName(current.getProducts().getProductsPK().getFileName());
            current.getStoresHasProductsPK().setStoreName(current.getStores().getStoreName());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("StoresHasProductsUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (StoresHasProducts) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("StoresHasProductsDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public StoresHasProducts getStoresHasProducts(com.ubs.stores.ubs.StoresHasProductsPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = StoresHasProducts.class)
    public static class StoresHasProductsControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            StoresHasProductsController controller = (StoresHasProductsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "storesHasProductsController");
            return controller.getStoresHasProducts(getKey(value));
        }

        com.ubs.stores.ubs.StoresHasProductsPK getKey(String value) {
            com.ubs.stores.ubs.StoresHasProductsPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new com.ubs.stores.ubs.StoresHasProductsPK();
            key.setProduct(values[0]);
            key.setFileName(values[1]);
            key.setStoreName(values[2]);
            key.setSequence(Integer.parseInt(values[3]));
            return key;
        }

        String getStringKey(com.ubs.stores.ubs.StoresHasProductsPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getProduct());
            sb.append(SEPARATOR);
            sb.append(value.getFileName());
            sb.append(SEPARATOR);
            sb.append(value.getStoreName());
            sb.append(SEPARATOR);
            sb.append(value.getSequence());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof StoresHasProducts) {
                StoresHasProducts o = (StoresHasProducts) object;
                return getStringKey(o.getStoresHasProductsPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + StoresHasProducts.class.getName());
            }
        }

    }

}
