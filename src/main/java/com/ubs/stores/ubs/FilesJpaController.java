/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ubs.stores.ubs;

import com.ubs.stores.ubs.exceptions.IllegalOrphanException;
import com.ubs.stores.ubs.exceptions.NonexistentEntityException;
import com.ubs.stores.ubs.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author plpm
 */
public class FilesJpaController implements Serializable {

    public FilesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Files files) throws PreexistingEntityException, Exception {
        if (files.getProductsCollection() == null) {
            files.setProductsCollection(new ArrayList<Products>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Products> attachedProductsCollection = new ArrayList<Products>();
            for (Products productsCollectionProductsToAttach : files.getProductsCollection()) {
                productsCollectionProductsToAttach = em.getReference(productsCollectionProductsToAttach.getClass(), productsCollectionProductsToAttach.getProductsPK());
                attachedProductsCollection.add(productsCollectionProductsToAttach);
            }
            files.setProductsCollection(attachedProductsCollection);
            em.persist(files);
            for (Products productsCollectionProducts : files.getProductsCollection()) {
                Files oldFilesOfProductsCollectionProducts = productsCollectionProducts.getFiles();
                productsCollectionProducts.setFiles(files);
                productsCollectionProducts = em.merge(productsCollectionProducts);
                if (oldFilesOfProductsCollectionProducts != null) {
                    oldFilesOfProductsCollectionProducts.getProductsCollection().remove(productsCollectionProducts);
                    oldFilesOfProductsCollectionProducts = em.merge(oldFilesOfProductsCollectionProducts);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFiles(files.getFileName()) != null) {
                throw new PreexistingEntityException("Files " + files + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Files files) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Files persistentFiles = em.find(Files.class, files.getFileName());
            Collection<Products> productsCollectionOld = persistentFiles.getProductsCollection();
            Collection<Products> productsCollectionNew = files.getProductsCollection();
            List<String> illegalOrphanMessages = null;
            for (Products productsCollectionOldProducts : productsCollectionOld) {
                if (!productsCollectionNew.contains(productsCollectionOldProducts)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Products " + productsCollectionOldProducts + " since its files field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Products> attachedProductsCollectionNew = new ArrayList<Products>();
            for (Products productsCollectionNewProductsToAttach : productsCollectionNew) {
                productsCollectionNewProductsToAttach = em.getReference(productsCollectionNewProductsToAttach.getClass(), productsCollectionNewProductsToAttach.getProductsPK());
                attachedProductsCollectionNew.add(productsCollectionNewProductsToAttach);
            }
            productsCollectionNew = attachedProductsCollectionNew;
            files.setProductsCollection(productsCollectionNew);
            files = em.merge(files);
            for (Products productsCollectionNewProducts : productsCollectionNew) {
                if (!productsCollectionOld.contains(productsCollectionNewProducts)) {
                    Files oldFilesOfProductsCollectionNewProducts = productsCollectionNewProducts.getFiles();
                    productsCollectionNewProducts.setFiles(files);
                    productsCollectionNewProducts = em.merge(productsCollectionNewProducts);
                    if (oldFilesOfProductsCollectionNewProducts != null && !oldFilesOfProductsCollectionNewProducts.equals(files)) {
                        oldFilesOfProductsCollectionNewProducts.getProductsCollection().remove(productsCollectionNewProducts);
                        oldFilesOfProductsCollectionNewProducts = em.merge(oldFilesOfProductsCollectionNewProducts);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = files.getFileName();
                if (findFiles(id) == null) {
                    throw new NonexistentEntityException("The files with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Files files;
            try {
                files = em.getReference(Files.class, id);
                files.getFileName();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The files with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Products> productsCollectionOrphanCheck = files.getProductsCollection();
            for (Products productsCollectionOrphanCheckProducts : productsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Files (" + files + ") cannot be destroyed since the Products " + productsCollectionOrphanCheckProducts + " in its productsCollection field has a non-nullable files field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(files);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Files> findFilesEntities() {
        return findFilesEntities(true, -1, -1);
    }

    public List<Files> findFilesEntities(int maxResults, int firstResult) {
        return findFilesEntities(false, maxResults, firstResult);
    }

    private List<Files> findFilesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Files.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Files findFiles(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Files.class, id);
        } finally {
            em.close();
        }
    }

    public int getFilesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Files> rt = cq.from(Files.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
